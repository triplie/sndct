<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Product;
use App\Sku;
use App\Size;

class ProductController extends Controller
{
	public function index()
    {
	    $products = Product::paginate(12);
        return view('site.shop', ['products' => $products]);
    }

    public function show($url)
    {   
        $product = Product::where(['url' => $url])->firstOrFail();
        $skus = $product->skus()->get();
        return view('site.product', ['product' => $product, 'skus' => $skus]);
    }

}
