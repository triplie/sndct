<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
Use App\Page;

class AboutController extends Controller
{
   public function index() {
    	return view('site.about');
    }
}
