<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Cart;
use App\Http\Requests\ProfileRequest;
use Illuminate\Support\Facades\Auth;
use App\Product;
use App\ProductSize;
use App\User;
use App\Sku;
use App\Size;

class CartController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

	public function add(Request $request){
        $product = Product::find($request->id);
        $sku = Sku::where(['product_id' => $product->id, 'size_id' => $request->input('shop-size')])->first();
        if ($sku != null) {
            Cart::add([
                'id' => $sku->id,
                'name' => $product->name,
                'price' => $product->price,
                'quantity'=> $request->quantity,
                'attributes' => [
                                'image' => $product->image,
                                'product_id' => $product->id,
                                'size' => $sku->size->name,
                                ]
            ]);
        } else {
            return redirect()->back()->with('error', 'You need to choose a size');
        }
        return redirect()->back()->withSuccess("$product->name successfully added to your shopping cart");
    }
    
    public function index(User $user, Request $request) 
    {
        $user = Auth::user();
        $products = Cart::getContent();
        $productsSubTotal = Cart::getSubTotal();
        if($products->isEmpty()) {
            return back();
        }else{
            return view('site.cart-checkout', ['user' => $user, 'products' => $products, 'productsSubTotal' => $productsSubTotal]);
        }
    }

    public function cart()
    {
        $products = Cart::getContent();
        $productsSubTotal = Cart::getSubTotal();
        return view('site.cart', ['products' => $products, 'productsSubTotal' => $productsSubTotal]);
    }

    public function clear(){
        Cart::clear();
        return back();
    }

    public function remove($id) {
        Cart::remove($id);
        return back();  
    }
}
