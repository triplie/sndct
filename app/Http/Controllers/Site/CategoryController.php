<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Category;
use App\Product;
use App\CategoryProduct;

class CategoryController extends Controller
{
    public function show($url)
    {
        $category = Category::where(['url' => $url])->firstorFail();
        $products = $category->products()->paginate(12);

        return view('site.category', ['category' => $category, 'products' => $products]);
    }
}
