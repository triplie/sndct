<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Product;
use App\Category;

class HomeController extends Controller
{
    public function index() 
    {
        $products = Product::paginate(6);
    	return view('site.home', ['products' => $products]);
    }
}
