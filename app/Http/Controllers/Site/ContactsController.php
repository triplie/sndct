<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\ContactRequest;
Use App\Contact;

class ContactsController extends Controller
{  
  public function index() 
  {
    	return view('site.contacts');
  }
    
    public function store(ContactRequest $request)
    {
		$m = Contact::create($request->all());
	    return view('site.thankyou-contact');
    }

}
