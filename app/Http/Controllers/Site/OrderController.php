<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Cart;
use App\Product;
use App\Http\Requests\ProfileRequest;
use App\User;
use App\Order;

class OrderController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth');
    }
 
    public function createOrder(ProfileRequest $request)
    {
    	$user = User::find(Auth::id());
        $user->fill($request->all());
        $user->save();
    	$products = Cart::getContent();

        foreach($products as $product){  
            $order = new Order();
            $order->user_id = Auth::user()->id;
            $order->product_id = $product->attributes['product_id'];
            $order->size = $product->attributes['size'];
            $order->quantity = $product->quantity;
            Auth::user()->orders()->save($order);
        }
        Cart::clear();
        return view('site.thankyou-order');
    }
}
