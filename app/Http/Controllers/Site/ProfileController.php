<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\ProfileRequest;
use App\User;
use App\Order;
use App\Status;

class ProfileController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function show(User $user)
    {   
        $user = Auth::user();
        $orders = $user->orders()->get();
        return view('site.profile', ['user' => $user, 'orders' => $orders]);
    }

    public function update(ProfileRequest $request)
    { 
        $user = User::find(Auth::id());
        $user->fill($request->all());
        $user->save();

        return redirect()->back();
    }
}
