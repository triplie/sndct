<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
Use App\Page;

class PageController extends Controller
{
   public function show($slug)
    {
    	$page = Page::where(['slug' => $slug, 'published' => true])->firstOrFail();

    	if ($page) {	
    	   return view('site.page', ['page' => $page]);
    	}else{
    		abort('404');
    	}
    }
}
