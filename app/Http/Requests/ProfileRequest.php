<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProfileRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    protected function formatErrors(Validator $validator)
    {
        return $validator->errors()->all();
    }
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        // if ($this->method() == 'PUT') {
        //     $email_rules = 'required|string|max:100|unique:pages,slug,' . $this->get('id');
        //     $name_rules = 'required|string|max:100|unique:users,name' . $this->get('id');
        // } else {
        //     $email_rules = 'required|string|max:100|unique:pages,slug,';
        //     $name_rules = 'required|string|max:100|unique:users,name';
        // }
        return [
            'email' => 'required|email|max:100|unique:users,email,' .$this->user()->id,
            'name' => 'required|string|max:100',
            'country' => 'required|string|max:100',
            'posta' => 'required|numeric',
            'adress' => 'required',
            'phone' => 'required|regex:/([0-9]{3})([-]?)([0-9]{3})\2([0-9]{4})/',
            'lastName' => 'required|max:100',

        ];
        
    }
}
