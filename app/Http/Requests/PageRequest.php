<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;

class PageRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

protected function formatErrors(Validator $validator)
    {
        return $validator->errors()->all();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if ($this->method() == 'PUT') {
            $slug_rules = 'required|string|max:255|unique:pages,slug,' . $this->get('id');
        } else {
            $slug_rules = 'required|string|max:255|unique:pages,slug,';
        }
        return [
            'slug' => $slug_rules,
            'name' => 'required|max:100',
            'title' => 'max:100',
            'description' => 'max:255',
            'keywords' => 'max:255',
            'content' => 'required',
            'published' => 'boolean'
        ];
    }
}
