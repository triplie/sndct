<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Size extends Model
{
	protected $fillable = [ 'name' ];
	
    public function skus()
    {
        return $this->hasMany('App\Sku');
    }
}
