<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sku extends Model
{
	protected $fillable = [ 'product_id', 'size_id', 'barcode', 'in_stock'];

    public function product()
    {
        return $this->belongsTo('App\Product');
    }
    public function size()
    {
        return $this->belongsTo('App\Size');
    }
}
