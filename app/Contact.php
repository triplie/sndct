<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    protected $fiilable = [
    	'lastName', 
    	'firstName', 
    	'email', 
    	'subject', 
    	'message'
    ];
    
    protected $guarded = [
    '_method',
    '_token'
  ];
}
