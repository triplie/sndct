<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable = [ 'product_id', 'user_id', 'size', 'quantity', 'status_id'];

    public function users()
    {
        return $this->belongsTo('App\User');
    }
    public function product()
    {
        return $this->belongsTo('App\Product');
    }
    public function status()
    {
        return $this->belongsTo('App\Status');
    }
}
