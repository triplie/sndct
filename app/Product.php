<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Product extends Model
{
    protected $fillable = [ 'name', 'category_id', 'size_id', 'sku_id', 'oldPrice', 'description', 'price', 'image',  'url', 'material', 'keywords'];

    public function categories()
    {
        return $this->belongsToMany('App\Category');
    }
       
    public function skus()
    {
        return $this->hasMany('App\Sku');
    }
}