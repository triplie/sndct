<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('description')->nullable();
            $table->string('keywords')->nullable();
            $table->string('material', 100)->nullable();
            $table->string('image');
            $table->string('url');
            $table->decimal('price', 8, 2);
            $table->timestamps();
        });
        Schema::table('products', function (Blueprint $table) {
            $table->string('image')->nullable()->change();
            $table->decimal('oldPrice', 8, 2)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
