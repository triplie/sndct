<?php

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->insert([
            [
               'url' => 'women',
               'title' => 'Women',
               'description' => 'clothes for women',
               
            ],
            [
               'url' => 'women-tops',
               'title' => 'Women-tops',
               'description' => 'clothes for women',
               
            ],
            [
               'url' => 'women-bottoms',
               'title' => 'Women-bottoms',
               'description' => 'clothes for women',
               
            ],
            [
               'url' => 'women-accesories',
               'title' => 'Women-accesories',
               'description' => 'clothes for women',
               
            ],
            [
               'url' => 'men',
               'title' => 'Men',
               'description' => 'clothes for men',
            ],
            [
               'url' => 'men-tops',
               'title' => 'Men-tops',
               'description' => 'clothes for men',
            ],
            [
               'url' => 'men-bottoms',
               'title' => 'Men-bottoms',
               'description' => 'clothes for men',
            ],
            [
               'url' => 'children',
               'title' => 'Children',
               'description' => 'clothes for children',
            ],
            [
               'url' => 'girls-8-14+',
               'title' => 'girls 8-14+ years',
               'description' => 'clothes for children',
            ],
            [
               'url' => 'boys-8-14+',
               'title' => 'boys 8-14+ years',
               'description' => 'clothes for children',
            ],
        ]);
    }
}
