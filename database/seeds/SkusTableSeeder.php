<?php

use Illuminate\Database\Seeder;

class SkusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('skus')->insert([
            [
               'in_stock' => '1',
               'barcode' => '1235536756',
               'product_id' => '1',
               'size_id' => '1',
            ],
            [
               'in_stock' => '1',
               'barcode' => '1235536756',
               'product_id' => '1',
               'size_id' => '3',
            ],
            [
               'in_stock' => '1',
               'barcode' => '1235536756',
               'product_id' => '2',
               'size_id' => '2',
            ],
            [
               'in_stock' => '1',
               'barcode' => '1235536756',
               'product_id' => '2',
               'size_id' => '1',
            ],
            [
               'in_stock' => '1',
               'barcode' => '1235536756',
               'product_id' => '3',
               'size_id' => '3',
            ],
            [
               'in_stock' => '1',
               'barcode' => '1235536756',
               'product_id' => '3',
               'size_id' => '1',
            ],
            [
               'in_stock' => '1',
               'barcode' => '1235536756',
               'product_id' => '4',
               'size_id' => '1',
            ],
            [
               'in_stock' => '1',
               'barcode' => '1235536756',
               'product_id' => '4',
               'size_id' => '2',
            ],
            [
               'in_stock' => '1',
               'barcode' => '1235536756',
               'product_id' => '5',
               'size_id' => '5',
            ],
            [
               'in_stock' => '1',
               'barcode' => '1235536756',
               'product_id' => '5',
               'size_id' => '6',
            ],
            [
               'in_stock' => '1',
               'barcode' => '1235536756',
               'product_id' => '10',
               'size_id' => '7',
            ],
            [
               'in_stock' => '1',
               'barcode' => '1235536756',
               'product_id' => '10',
               'size_id' => '8',
            ],
            [
               'in_stock' => '1',
               'barcode' => '1235536756',
               'product_id' => '9',
               'size_id' => '9',
            ],
            [
               'in_stock' => '1',
               'barcode' => '1235536756',
               'product_id' => '8',
               'size_id' => '2',
            ],
            [
               'in_stock' => '1',
               'barcode' => '1235536756',
               'product_id' => '8',
               'size_id' => '4',
            ],
            [
               'in_stock' => '1',
               'barcode' => '1235536756',
               'product_id' => '8',
               'size_id' => '1',
            ],
            [
               'in_stock' => '1',
               'barcode' => '1235536756',
               'product_id' => '7',
               'size_id' => '2',
            ],
            [
               'in_stock' => '1',
               'barcode' => '1235536756',
               'product_id' => '7',
               'size_id' => '3',
            ],
            [
               'in_stock' => '1',
               'barcode' => '1235536756',
               'product_id' => '6',
               'size_id' => '2',
            ],
            [
               'in_stock' => '1',
               'barcode' => '1235536756',
               'product_id' => '6',
               'size_id' => '1',
            ],
            [
               'in_stock' => '1',
               'barcode' => '1235536756',
               'product_id' => '6',
               'size_id' => '4',
            ],
            [
               'in_stock' => '1',
               'barcode' => '1235536756',
               'product_id' => '11',
               'size_id' => '11',
            ],
            [
               'in_stock' => '1',
               'barcode' => '1235536756',
               'product_id' => '11',
               'size_id' => '10',
            ],
            [
               'in_stock' => '1',
               'barcode' => '1235536756',
               'product_id' => '12',
               'size_id' => '11',
            ],
            [
               'in_stock' => '1',
               'barcode' => '1235536756',
               'product_id' => '12',
               'size_id' => '10',
            ],
            [
               'in_stock' => '1',
               'barcode' => '1235536756',
               'product_id' => '13',
               'size_id' => '7',
            ],
            [
               'in_stock' => '1',
               'barcode' => '1235536756',
               'product_id' => '13',
               'size_id' => '8',
            ],
        ]);
    }
}
