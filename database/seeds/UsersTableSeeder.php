<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            [
               'name' => 'admin',
               'email' => 'admin@admin.com',
               'password' => '$2y$10$fo2Y1PWjeof88/bl5p4uZ.2sODnVgD0pU/U0mxAcAsqfpe98QTnAm',
               
            ],
            [
               'name' => '1',
               'email' => 'xomenko.artem@ukr.net',
               'password' => '$2y$10$zjhFfHObLSyd7dfxCWUMvuXC8QBclCnneemsz/tQe2rGJavhz5AEO',
            ]
        ]);
    }
}
