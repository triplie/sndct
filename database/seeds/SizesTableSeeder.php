<?php

use Illuminate\Database\Seeder;

class SizesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('sizes')->insert([
            [
               'name' => 'X',                          
            ],
            [
               'name' => 'XL',
            ],
            [
               'name' => 'L',
            ],
            [
               'name' => 'S',                          
            ],
            [
               'name' => '160/80A',                          
            ],
            [
               'name' => '165/88A',                          
            ],
            [
               'name' => '170/96A',                          
            ],
            [
               'name' => '170/102A',                          
            ],
            [
               'name' => 'nosize',                          
            ],
            [
               'name' => '150/76',                          
            ],
            [
               'name' => '170/88',                          
            ],
        ]);
    }
}
