<?php

use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('products')->insert([
            [
               'name' => 'shirt-v1',
               'price' => '19.99',
               'material' => 'cotton',
               'url' => 'shirt-v1',
               'image' => 'products\August2020\eJEe6WgHWJvdhVMBWENQ.jpg'
            ],
            [
              'name' => 'shirt basic',
               'price' => '19.99',
               'material' => 'cotton',
               'url' => 'basic-shirt',
               'image' => 'products\August2020\In8SxsfemJV56cnfZlSX.jpg'
            ],
            [
               'name' => 't-shirt',
               'price' => '14.99',
               'material' => 'cotton',
               'url' => 't-shirt',
               'image' => 'products\August2020\icX3bIg4pYMVPqlwbFME.jpg'
            ],
            [
               'name' => 'hooded sweatshirt',
               'price' => '24.99',
               'material' => 'cotton',
               'url' => 'hooded-sweatshirt',
               'image' => 'products\August2020\cf3xtrkEzfdCGKO1JNjt.jpg'
            ],
            [
               'name' => 'skirt',
               'price' => '34.99',
               'material' => 'leather',
               'url' => 'skirt',
               'image' => 'products\August2020\3vpp5IJVOROs9lqZgx5g.jpg'
            ],
            [
               'name' => 'hoodie black',
               'price' => '34.99',
               'material' => 'cotton',
               'url' => 'hoodie-black',
               'image' => 'products\August2020\KoQnfeuHKLWDd2DoT80O.jpg'
            ],
            [
               'name' => 'sweatshirt',
               'price' => '24.99',
               'material' => 'cotton',
               'url' => 'sweatshirt',
               'image' => 'products\August2020\zjEj3x9FyBDTUz0EskHM.jpg'
            ],
            [
               'name' => 't-shirt',
               'price' => '14.99',
               'material' => 'cotton',
               'url' => 'tshirt-v1',
               'image' => 'products\August2020\jbJtoltHSECI5WS4MjUn.jpg'
            ],
            [
               'name' => 'scarf',
               'price' => '9.99',
               'material' => 'polyester',
               'url' => 'scarf',
               'image' => 'products\August2020\Z3BKh3bZfB9YZ0yzVKnq.jpg'
            ],
            [
               'name' => 'jeans',
               'price' => '49.99',
               'material' => 'cotton',
               'url' => 'jeans',
               'image' => 'products\August2020\XRdJupN9Rvv0CBnQnlPu.jpg'
            ],
            [
               'name' => 'Knitted cardigan',
               'price' => '39.99',
               'material' => 'cotton',
               'url' => 'cardigan',
               'image' => 'products\August2020\I7lRHaZENOvPZnx4KJKT.jpg'
            ],
            [
               'name' => 'Pocket-detail jumper',
               'price' => '29.99',
               'material' => 'cotton',
               'url' => 'jumper',
               'image' => 'products\August2020\j1FypDEY0D6RhG1twHP5.jpg'
            ],
            [
               'name' => 'Slim Straight Jeans',
               'price' => '49.99',
               'material' => 'cotton',
               'url' => 'slim-straight-jeans',
               'image' => 'products\August2020\zTFhLLKL3c9rOTTfTqsg.jpg'
            ],
        ]);
    }
}
