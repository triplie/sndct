<?php

use Illuminate\Database\Seeder;

class StatusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       DB::table('statuses')->insert([
           [
               'name' => 'new',
           ],
           [
               'name' => 'in_processing',
           ],
           [
               'name' => 'sended',
           ],
           [
               'name' => 'completed',
           ],
           [
               'name' => 'canceled',
           ],
       ]);
    }
}
