<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Auth::routes();
Route::group(['middleware' => 'auth'], function () {
	Route::get('/admin/dashboard', 'Admin\DashboardController@index')->name('dashboard');
	// Route::get('/admin/dashboard', 'Admin\DashboardController@index')->name('dashboard');
	// Route::get('/admin/page/create', 'Admin\PageController@create')->name('page.create'); 
	// Route::get('/admin/pages', 'Admin\PageController@index')->name('page.index'); 
	// Route::get('/admin/pages/{id}', 'Admin\PageController@show')->name('page.show'); 
	// Route::post('/admin/pages', 'Admin\PageController@store')->name('page.store'); 
	// Route::get('/admin/page/edit/{id}', 'Admin\PageController@edit')->name('page.edit'); 
	// Route::put('/admin/pages/{id}', 'Admin\PageController@update')->name('page.update');
	// Route::delete('/admin/pages/{id}', 'Admin\PageController@destroy')->name('page.destroy');
});
Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});

Route::group(['namespace' => 'Site'], function () {
	Route::post('/cart-add', 'CartController@add')->name('cart.add');
	Route::get('/cart-checkout', 'CartController@index')->name('cart.checkout');
	Route::get('/cart', 'CartController@cart')->name('cart');
	Route::post('/cart-clear', 'CartController@clear')->name('cart.clear');
	Route::get('/cart/{id}/remove', 'CartController@remove')->name('cart.remove');

	Route::get('/category/{url}', 'CategoryController@show')->name('category.show');

	Route::get('/profile', 'ProfileController@show')->name('profile');
	Route::put('/profile/update', 'ProfileController@update')->name('profile.update');

	Route::get('/shop', 'ProductController@index')->name('shop');
	Route::get('/products/{url}', 'ProductController@show')->name('products.show');

	Route::put('/order', 'OrderController@createOrder')->name('order');

	Route::get('/contacts', 'ContactsController@index')->name('contacts');
	Route::post('/contacts', 'ContactsController@store')->name('contacts.store');

	Route::get('/about', 'AboutController@index')->name('about');
});

Route::get('/{slug}', 'Site\PageController@show')->name('page');
Route::get('/', 'Site\HomeController@index')->name('home');


