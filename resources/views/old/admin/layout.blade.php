<!DOCTYPE html>
<html>
<head>
	<title>Admin_ . @yield('title')</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
	<style>

		html, body{
			height: 100vh;
            margin: 0;
		}
		.header{
			width: 1039px;
height: 141px;
left: 442px;
top: 15px;

			position: relative;
		}
		.center-header{
position: absolute;
width: 340px;
height: 141px;
left: 790px;
top: 15px;

		}
		.center-header > img {
			width: 69px;
			height: 69px;
			left: 926px;
			top: 15px;

		}

	</style>
</head>
<body>
	<header>
		<div class="header">
			<div class="center-header">
				<!-- <img src="{{ asset('letter-s.svg') }}"> -->
			</div>
		
		</div>
	</header>

	<main>	
	@yield('content')
	</main>

	<aside>
		@section('sidebar')
		<!-- <p>Это дополнение к основной боковой панели</p> -->
		@show
	</aside>

	<footer>

	</footer>
	<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
	<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
</body>
</html>