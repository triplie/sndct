@extends('admin.layout')

@section('sidebar')
    @parent
@endsection

@section('content')
	<h1>Создание статьи</h1>
	
{!! Form::open(['url' => '/admin/pages', 'method' => 'post']) !!}
	@include('admin.page.form')
{!! Form::close() !!}

@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
@endsection