@extends('admin.layout')

@section('sidebar')
    @parent

    <p></p>
@endsection

@section('content')
	<h1>Сторінки</h1>
	<ul>
		@foreach($pages as $page)
			@include('admin.list-item', ['item' => $page])
		@endforeach
	</ul>
@endsection

{{ $pages->links() }}
