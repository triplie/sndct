<div>
	{{ Form::label('slug', 'Slug') }}
	{{ Form::text('slug', isset($page) ? $page->slug : '') }}
</div>
<div>
	{{ Form::label('name', 'Name') }}
	{{ Form::text('name', isset($page) ? $page->name : '') }}
</div>
<div>
	{{ Form::label('content', 'Content') }}
	{{ Form::textarea('content', isset($page) ? $page->content : '', ['rows' => 10]) }}
</div>
<div>
	<label>
		{{ Form::hidden('published', 0) }}
		{{ Form::checkbox('published', isset($page) ? $page->published : 0) }}
		Published
	</label>
</div>
<div>
	{{ Form::submit('Send') }}
</div>
