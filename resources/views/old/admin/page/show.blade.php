@extends('admin.layout')

@section('title', $page->name)

@section('content')
<h1>{{ $page->name }}</h1>

{!! Form::open(['url' => '/admin/pages/' . $page->id, 'method' => 'delete']) !!}
		{{ Form::submit('Delete') }}
{!! Form::close() !!}
@endsection