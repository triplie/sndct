@extends('layouts.site-layout')
@section('content')
  <style type="text/css">
    .out-of-stock{
      text-decoration: line-through;
    }
  </style>
    <div class="bg-light py-3">
      <div class="container">
        <div class="row">
          <div class="col-md-12 mb-0">
            <a href="{{route('home')}}">Home</a>
            <span class="mx-2 mb-0">/</span> 
            <strong class="text-black">Tank Top T-Shirt</strong>
          </div>
        </div>
      </div>
    </div>  

    <div class="site-section">
      <div class="container">
        <div class="row">
          <div class="col-md-1"></div>
          <div class="col-md-6">
            <img class="img-fluid" width="400" src="{{asset('storage/' . $product->image)}}" alt="Image">
          </div>
          <div class="col-md-5">
            <div class="flash-message alert-success">
              @if( Session::has( 'success' ))
                {{ Session::get( 'success' ) }}
              @endif
            </div>
            <h2 class="text-black">{{$product->name}}</h2>
            <p>{{$product->description}}</p>
            <p class="mb-4"><b>Material: </b>{{$product->material}}</p>
            <p><strong class="text-primary h4">{{'$' . $product->price}}</strong></p>
              <form method="POST" action="{{route('cart.add')}}">
                @csrf
                <div class="alert-danger flash-message">
                  @if( Session::has( 'error' ))
                    {{ Session::get( 'error' ) }}
                  @endif
                </div>
                <div class="mb-1 d-flex">
                  @foreach($skus as $sku)
                    @if($sku->in_stock == 1)
                    <label for="option-sm-{{$sku->size->id}}" class="d-flex mr-3 mb-3">
                      <span class="d-inline-block mr-1" >
                      <input type="radio" id="option-sm-{{$sku->size->id}}" name="shop-size" value="{{$sku->size->id}}">
                      </span>
                      <span class="d-inline-block text-black">{{$sku->size->name}}</span>
                    </label>
                    @endif
                    @if($sku->in_stock == 0)
                      <label for="option-sm-{{$sku->size->id}}" class="d-flex mr-3 mb-3">
                      <span class="d-inline-block text-black out-of-stock">{{$sku->size->name}}</span>
                    </label>
                    @endif
                  @endforeach
                </div>
                <div class="mb-5">
                  <div class="input-group mb-3" style="max-width: 150px;">
                    <div class="input-group-prepend">
                      <button class="btn btn-outline-primary js-btn-minus" type="button">&minus;</button>
                    </div>   
                    <input type="text" class="form-control text-center" value="1" name="quantity" 
                    onkeypress="if ( isNaN( String.fromCharCode(event.keyCode) )) return false;">
                    <div class="input-group-append">
                      <button class="btn btn-outline-primary js-btn-plus" type="button">&plus;</button>
                    </div>
                  </div>
                </div>
                <input name="id" type="hidden" value="{{$product->id}}">
                <button class="buy-now btn btn-sm btn-primary" type="submit">Add to cart</button>
              </form>
          </div>
        </div>
      </div>
    </div> 
@endsection