@extends('layouts.site-layout')
@section('content')
    <div class="bg-light py-3">
      <div class="container">
        <div class="row">
          <div class="col-md-12 mb-0"><a href="{{route('shop')}}">Home</a> <span class="mx-2 mb-0">/</span><a href="{{route('shop')}}">Shop</a> <span class="mx-2 mb-0">/</span>  <strong class="text-black">Category: {{$category->title}}</strong></div>
        </div>
      </div>
    </div>

    <div class="hero-wrap hero-bread" style="background-image: url('/images/bg_6.jpg');">
      <div class="container">
        <div class="row no-gutters slider-text align-items-center justify-content-center">
          <div class="col-md-9 ftco-animate text-center">
            <h1 class="mb-0 bread">{{$category->title}}</h1>
          </div>
        </div>
      </div>
    </div>

    <section class="ftco-section bg-light">
      <div class="container-fluid">
        <div class="row">
          @foreach($products as $product)
            <div class="col-sm col-md-6 col-lg-3 ftco-animate">
              <div class="product">
                <a href="{{route('products.show', ['url' => $product->url]) }}" class="img-prod"><img class="img-fluid" src="{{asset('storage/' . $product->image)}}"></a>
                <div class="text py-3 px-3">
                  <h3><a href="{{route('products.show', ['url' => $product->url]) }}">{{$product->name}}</a></h3>
                  <div class="d-flex">
                    <div class="pricing">
                      <p class="price">
                        <span class="mr-2 price-dc">{{isset($product->oldPrice) != 0 ? $product->oldPrice . '$' : ''}}</span>
                        <span class="price-sale">{{$product->price . '$'}}</span>
                      </p>
                    </div>
                  </div>
                </div>
              </div>
            </div> 
          @endforeach
        </div>

        <div class="row mt-5">
          <div class="col text-center">
            <div class="block-27">
              {{ $products->links('layouts.paginate') }}
            </div>
          </div>
        </div>
      </div>
    </section>

@endsection

@section('footer')
  <script src="/shopcss/js/jquery.min.js"></script>
  <script src="/shopcss/js/jquery-migrate-3.0.1.min.js"></script>
  <script src="/shopcss/js/popper.min.js"></script>
  <script src="/shopcss/js/bootstrap.min.js"></script>
  <script src="/shopcss/js/jquery.easing.1.3.js"></script>
  <script src="/shopcss/js/jquery.waypoints.min.js"></script>
  <script src="/shopcss/js/jquery.stellar.min.js"></script>
  <script src="/shopcss/js/owl.carousel.min.js"></script>
  <script src="/shopcss/js/jquery.magnific-popup.min.js"></script>
  <script src="/shopcss/js/aos.js"></script>
  <script src="/shopcss/js/jquery.animateNumber.min.js"></script>
  <script src="/shopcss/js/bootstrap-datepicker.js"></script>
  <script src="/shopcss/js/scrollax.min.js"></script>
  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBVWaKrjvy3MaE7SQ74_uJiULgl1JY0H2s&sensor=false"></script>
  <script src="/shopcss/js/google-map.js"></script>
  <script src="/shopcss/js/main.js"></script>
 @endsection

<link href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700,900" rel="stylesheet">
<link rel="stylesheet" href="/shopcss/css/animate.css">
<link rel="stylesheet" href="/shopcss/css/style.css">

