@extends('layouts.site-layout')
@section('content')

    <div class="bg-light py-3">
      <div class="container">
        <div class="row">
          <div class="col-md-12 mb-0"><a href="{{route('home')}}">Home</a> <span class="mx-2 mb-0">/</span> <a href="{{route('cart')}}">Cart</a> <span class="mx-2 mb-0">/</span> <strong class="text-black">Checkout</strong></div>
        </div>
      </div>
    </div>

    <div class="site-section">
      <div class="container">
        <div class="row">
          <div class="col-md-6 mb-5 mb-md-0">
            <h2 class="h3 mb-3 text-black">Billing Details</h2>
            <form method="post" class="form-group row" action="/order">
              @csrf
              <div class="p-3 p-lg-5 border">
                <div class="form-group row"> 
                  <div class="col-md-6">
                    {{ Form::label('name', 'First Name', ['class' => 'text-black']) }}
                    <span class="text-danger">*</span>
                    {{ Form::text('name', isset($user) ? $user->name : '', ['class' => 'form-control', 'id'=>'c_fname']) }}
                    {{ Form::hidden('_method', 'put') }}
                  </div>

                  <div class="col-md-6">
                    {{ Form::label('lastName', 'Last Name', ['class' => 'text-black']) }}
                    <span class="text-danger">*</span>
                    {{ Form::text('lastName', isset($user) ? $user->lastName : '', ['class'=>'form-control', 'id'=>'c_lname']) }}
                  </div>
                </div>

                <div class="form-group row">
                  <div class="col-md-12">             
                    {{ Form::label('adress', 'Your Address', ['class' => 'text-black']) }}
                    <span class="text-danger">*</span>
                    {{ Form::textarea('adress', isset($user) ? $user->adress : '', ['class' => 'form-control','rows'=>2, 'id'=>'c_address']) }}
                  </div>
                </div>

                <div class="form-group row">
                  <div class="col-md-6">
                    {{ Form::label('country', 'State / Country', ['class' => 'text-black']) }}
                    <span class="text-danger">*</span>
                    {{ Form::text('country', isset($user) ? $user->country : '', ['class'=>'form-control', 'id'=>'c_state_country']) }}
                  </div>

                  <div class="col-md-6">
                      {{ Form::label('posta', 'Posta / Zip', ['class' => 'text-black']) }}
                      <span class="text-danger">*</span>
                      {{ Form::text('posta', isset($user) ? $user->posta  : '', ['class' => 'form-control', 'id'=>'c_postal_zip']) }}
                  </div>
                </div>

                <div class="form-group row mb-5">
                  <div class="col-md-6">
                    {{ Form::label('email', 'Your Email', ['class' => 'text-black']) }}
                    <span class="text-danger">*</span>
                    {{ Form::email('email', isset($user) ? $user->email : '', ['class' => 'form-control', 'id'=>'c_email_address']) }}
                  </div>

                  <div class="col-md-6">
                      {{ Form::label('phone', 'Your Phone number', ['class' => 'text-black']) }}
                      <span class="text-danger">*</span>
                      {{ Form::text('phone', isset($user) ? $user->phone : '', ['class' => 'form-control', 'id'=>'phone']) }}
                  </div>
                </div>
                @if ($errors->any())
                  <div class="alert alert-danger">
                    <ul>
                      @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                      @endforeach
                    </ul>
                  </div>
                @endif
              </div>
            </div>

            <div class="col-md-6">
              <div class="row mb-5">
                <div class="col-md-12">
                  <h2 class="h3 mb-3 text-black">Your Order</h2>
                  <div class="p-3 p-lg-5 border">
                    <table class="table site-block-order-table mb-5">
                      <thead>
                        <th>Product</th>
                        <th>Price</th>
                      </thead>
                      <tbody>
                        @foreach($products as $product)
                          <tr>
                            <td>{{$product->name}}<strong class="mx-2">x</strong>{{$product->quantity}}</td>
                            <td>{{$product->price . '  $'}}</td>
                          </tr>
                        @endforeach
                        <tr>
                          <td class="text-black font-weight-bold"><strong>Order Total</strong></td>
                          <td class="text-black font-weight-bold"><strong>{{$productsSubTotal . '  $'}}</strong></td>
                        </tr>
                      </tbody>
                    </table>
                    
                    <div class="border p-3 mb-3">
                      <h3 class="h6 mb-0"><a class="d-block" data-toggle="collapse" href="#collapsecheque" role="button" aria-expanded="false" aria-controls="collapsecheque">Cheque Payment</a></h3>
                      <div class="collapse" id="collapsecheque">
                        <div class="py-2">
                          <p class="mb-0">Make your payment directly into our bank account. Please use your Order ID as the payment reference. Your order won’t be shipped until the funds have cleared in our account.</p>
                        </div>
                      </div>
                    </div>

                    <div class="border p-3 mb-5">
                      <h3 class="h6 mb-0"><a class="d-block" data-toggle="collapse" href="#collapsepaypal" role="button" aria-expanded="false" aria-controls="collapsepaypal">Paypal</a>
                      </h3>
                      <div class="collapse" id="collapsepaypal">
                        <div class="py-2">
                          <p class="mb-0">Make your payment directly into our bank account. Please use your Order ID as the payment reference. Your order won’t be shipped until the funds have cleared in our account.</p>
                        </div>
                      </div>
                    </div>
                    <div class="form-group">                               
                        <button class="btn btn-primary btn-lg py-3 btn-block" type="submit">Place Order</button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
@endsection

@section('footer')
    <script type="text/javascript">
      function phone_formatting(ele,restore) {
      var new_number,
          selection_start = ele.selectionStart,
          selection_end = ele.selectionEnd,
          number = ele.value.replace(/\D/g,'');
      
      // automatically add dashes
      if (number.length > 2) {
        // matches: 123 || 123-4 || 123-45
        new_number = number.substring(0,3) + '-';
        if (number.length === 4 || number.length === 5) {
          // matches: 123-4 || 123-45
          new_number += number.substr(3);
        }
        else if (number.length > 5) {
          // matches: 123-456 || 123-456-7 || 123-456-789
          new_number += number.substring(3,6) + '-';
        }
        if (number.length > 6) {
          // matches: 123-456-7 || 123-456-789 || 123-456-7890
          new_number += number.substring(6);
        }
      }
      else {
        new_number = number;
      }
      
      // if value is heigher than 12, last number is dropped
      // if inserting a number before the last character, numbers
      // are shifted right, only 12 characters will show
      ele.value =  (new_number.length > 12) ? new_number.substring(12,0) : new_number;
      
      // restore cursor selection,
      // prevent it from going to the end
      // UNLESS
      // cursor was at the end AND a dash was added
      document.getElementById('msg').innerHTML='<p>Selection is: ' + selection_end + ' and length is: ' + new_number.length + '</p>';
      
      if (new_number.slice(-1) === '-' && restore === false
          && (new_number.length === 8 && selection_end === 7)
              || (new_number.length === 4 && selection_end === 3)) {
          selection_start = new_number.length;
          selection_end = new_number.length;
      }
      else if (restore === 'revert') {
        selection_start--;
        selection_end--;
      }
      ele.setSelectionRange(selection_start, selection_end);

    }
      
    function phone_number_check(field,e) {
      var key_code = e.keyCode,
          key_string = String.fromCharCode(key_code),
          press_delete = false,
          dash_key = 189,
          delete_key = [8,46],
          direction_key = [33,34,35,36,37,38,39,40],
          selection_end = field.selectionEnd;
      
      // delete key was pressed
      if (delete_key.indexOf(key_code) > -1) {
        press_delete = true;
      }
      
      // only force formatting is a number or delete key was pressed
      if (key_string.match(/^\d+$/) || press_delete) {
        phone_formatting(field,press_delete);
      }
      // do nothing for direction keys, keep their default actions
      else if(direction_key.indexOf(key_code) > -1) {
        // do nothing
      }
      else if(dash_key === key_code) {
        if (selection_end === field.value.length) {
          field.value = field.value.slice(0,-1)
        }
        else {
          field.value = field.value.substring(0,(selection_end - 1)) + field.value.substr(selection_end)
          field.selectionEnd = selection_end - 1;
        }
      }
      // all other non numerical key presses, remove their value
      else {
        e.preventDefault();
    //    field.value = field.value.replace(/[^0-9\-]/g,'')
        phone_formatting(field,'revert');
      }

    }

    document.getElementById('phone').onkeyup = function(e) {
      phone_number_check(this,e);
    }
    </script>
@endsection