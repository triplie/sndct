@extends('layouts.site-layout')
@section('content')
    <div class="bg-light py-3">
      <div class="container">
        <div class="row">
          <div class="col-md-12 mb-0"><a href="{{route('home')}}">Home</a> <span class="mx-2 mb-0">/</span> <strong class="text-black">Cart</strong></div>
        </div>
      </div>
    </div>

    <div class="site-section">
      <div class="container">
        <div class="row mb-5">
          <form class="col-md-12" method="post">
            <div class="site-blocks-table">
              <table class="table table-bordered">
                <thead>
                  <tr>
                    <th class="product-thumbnail">Image</th>
                    <th class="product-name">Product</th>
                    <th class="product-price">Price</th>
                    <th class="product-quantity">Quantity</th>
                    <th class="product-total">Size</th>
                    <th class="product-remove">Remove</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($products as $product)
                    <tr>
                      <td class="product-thumbnail">
                        <img src="{{asset('storage/' . $product->attributes['image'])}}" alt="Image" class="img-fluid">
                      </td>
                      <td class="product-name">
                        <h2 class="h5 text-black">{{$product->name}}</h2>
                      </td>
                      <td><span class="text-center h5">{{'$' . $product->price}}</span></td> 
                      <td>
                         <small class="h5 text-center">{{$product->quantity}}</small>
                      </td>
                      <td><div class="h5">{{$product->attributes['size']}}</div></td>
                      <td>
                          <a href="{{ route('cart.remove', $product->id) }}" class="btn btn-outline-danger"><i class="fa fa-times"></i> X</a>
                      </td>
                    </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </form>
        </div>

        <div class="row">
          <div class="col-md-6">
            <div class="row mb-5">
              <div class="col-md-6">
                <a class="btn btn-outline-primary btn-sm btn-block" href="{{route('shop')}}">Continue Shopping</a>
              </div>
            </div>
          
          </div>
          <div class="col-md-6 pl-5">
            <div class="row justify-content-end">
              <div class="col-md-7">
                <div class="row">
                  <div class="col-md-12 text-right border-bottom mb-5">
                    <h3 class="text-black h4 text-uppercase">Cart Totals</h3>
                  </div>
                </div>
                
                <div class="row mb-5">
                  <div class="col-md-6">
                    <span class="text-black">Total</span>
                  </div>
                  <div class="col-md-6 text-right">
                    <strong class="text-black">{{$productsSubTotal . '$'}}</strong>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                    <a href="{{route('cart.checkout')}}"><button class="btn btn-primary btn-lg py-3 btn-block">Proceed To Checkout</button></a>
                      <form action="{{route('cart.clear')}}" method="POST" class="card ">
                        @csrf
                        <button type="submit" class="btn btn-danger">Clear Cart</button>
                      </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
@endsection