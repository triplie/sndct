<!DOCTYPE html>
<html>
  <head>
    <title>Syndicate</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Mukta:300,400,700"> 
    <link rel="stylesheet" href="/fonts/icomoon/style.css">

    <link rel="stylesheet" href="/css/bootstrap.min.css">
    <link rel="stylesheet" href="/css/magnific-popup.css">
    <link rel="stylesheet" href="/css/jquery-ui.css">
    <link rel="stylesheet" href="/css/owl.carousel.min.css">
    <link rel="stylesheet" href="/css/owl.theme.default.min.css">
    <link rel="stylesheet" href="/css/aos.css">
    <link rel="stylesheet" href="/css/style.css">
    <link rel="icon" href="letter-s.svg">
    @yield('header')
  </head>
  <body>
  <div class="site-wrap">
    <header class="site-navbar" role="banner">
      <div class="site-navbar-top">
        <div class="container">
          <div class="row align-items-center">
            <div class="col-6 col-md-4 order-2 order-md-1 text-left block-5" >
              <ul>
              <li class="phone"><a href="tel://23923929210">+2 392 3929 210</a></li>
              </ul>
            </div>
            <div class="col-12 mb-3 mb-md-0 col-md-4 order-1 order-md-2 text-center">
              <div class="site-logo">
                <a href="{{route('home')}}" class="js-logo-clone">Syndicate</a>
              </div>
            </div>
            <div class="col-6 col-md-4 order-3 order-md-3 text-right">
            <div class="site-top-icons">
                <ul>
                  <li><a href="{{route('profile')}}"><span class="icon icon-person"></span></a></li>
                  <li>
                    <a href="{{route('cart')}}" class="site-cart">
                      <span class="icon icon-shopping_cart"></span>
                      <span class="count">{{Cart::getContent()->count()}}</span>
                    </a>
                  </li> 
                </ul>
              </div>  
            </div>
          </div>
        </div>
      </div> 
      <nav class="site-navigation text-right text-md-center" role="navigation">
        <div class="container">
          <ul class="site-menu js-clone-nav d-none d-md-block">
            <li><a href="{{route('home')}}">Home</a></li>
            <li class="has-children">
              <a href="{{route('shop')}}">Shop</a>
              <ul class="dropdown">
                <li class="has-children">
                  <a href="{{route('category.show', ['url' => 'men']) }}">Men</a>
                  <ul class="dropdown">
                    <li><a href="{{route('category.show', ['url' => 'men']) }}">View all</a></li>
                    <li><a href="{{route('category.show', ['url' => 'men-tops']) }}">Tops</a></li>
                    <li><a href="{{route('category.show', ['url' => 'men-bottoms']) }}">Bottoms</a></li>
                  </ul>
                </li>

                <li class="has-children">
                  <a href="{{route('category.show', ['url' => 'women']) }}">Women</a>
                  <ul class="dropdown">
                    <li><a href="{{route('category.show', ['url' => 'women']) }}">View all</a></li>
                    <li><a href="{{route('category.show', ['url' => 'women-tops']) }}">Tops</a></li>
                    <li><a href="{{route('category.show', ['url' => 'women-bottoms']) }}">Bottoms</a></li>
                    <li><a href="{{route('category.show', ['url' => 'women-accesories']) }}">Accesories</a></li>
                  </ul>
                </li>

                <li class="has-children">
                  <a href="{{route('category.show', ['url' => 'children']) }}">Children</a>
                  <ul class="dropdown">
                    <li><a href="{{route('category.show', ['url' => 'children']) }}">View all</a></li>
                    <li><a href="{{route('category.show', ['url' => 'girls-8-14+']) }}">Girls 8-14+ years</a></li>
                    <li><a href="{{route('category.show', ['url' => 'boys-8-14+']) }}">Boys 8-14+ years</a></li>
                  </ul>
                </li>
              </ul>
            </li>
            <li><a href="{{route('about')}}">About</a></li>
            <li><a href="{{route('contacts')}}">Contacts</a></li>
          </ul>
        </div>
      </nav>
    </header>

    <content>
    	@yield('content')
    </content>

    <footer class="site-footer border-top">
      <div class="container">
        <div class="row">
          <div class="col-lg-4 mb-5 mb-lg-0">
            <div class="row">
              <div class="col-md-12">
                <h3 class="footer-heading mb-3">Navigations</h3>
              </div>
              <div class="col-md-6 col-lg-4">
                <ul class="list-unstyled">
                  <li><a href="{{route('home')}}">Home</a></li>
                  <li><a href="{{route('contacts')}}">Contacts</a></li>
                  <li><a href="{{route('shop') }}">Shop</a></li>
                </ul>
              </div>
              <div class="col-md-6 col-lg-4">
                <ul class="list-unstyled">
                  <li><a href="https://www.instagram.com/">Our Instagram</a></li>
                  <li><a href="https://telegram.org/">Our Telegram</a></li>
                  <li><a href="https://www.facebook.com/">Our Facebook</a></li>
                </ul>
              </div>
            </div>
          </div>
          <div class="col-md-6 col-lg-5 mb-5 mb-lg-0">
              <div class="block-7">
                <h3 class="footer-heading mb-3">Our shops</h3>
                <iframe src="https://www.google.com/maps/embed?pb=!1m16!1m12!1m3!1d23110855.54627729!2d2.5774199465767964!3d45.00158876352622!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!2m1!1sgucci!5e0!3m2!1sru!2sua!4v1595418412147!5m2!1sru!2sua" width="310" height="200" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
              </div>
          </div>
          <div class="col-md-6 col-lg-3">
            <div class="block-5 mb-5">
              <h3 class="footer-heading mb-3">Contact Info</h3>
              <ul class="list-unstyled">
                <li class="address">203 Fake St. Mountain View, San Francisco, California, USA</li>
                <li class="phone"><a href="tel://23923929210">+2 392 3929 210</a></li>
                <li class="email">emailaddress@domain.com</li>
              </ul>
            </div>  
          </div>
        </div>
      </div>
      <div class="row pt-5 mt-5 text-center">
        <div class="col-md-12">
          <p>
            <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
            <script data-cfasync="false" src="/cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script> All rights reserved &copy;
            <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
          </p>
        </div>

        <div class="col-md-12">
            <p>
            <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
            <script data-cfasync="false" src="/cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script>
            Syndicate™
            <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
            </p>
        </div>
      </div>
    </footer>
  </div>

    @yield('footer')
    <script src="/js/jquery-3.3.1.min.js"></script>
    <script src="/js/jquery-ui.js"></script>
    <script src="/js/popper.min.js"></script>
    <script src="/js/bootstrap.min.js"></script>
    <script src="/js/owl.carousel.min.js"></script>
    <script src="/js/jquery.magnific-popup.min.js"></script>
    <script src="/js/aos.js"></script>
    <script src="/js/main.js"></script>
  </body>
</html>